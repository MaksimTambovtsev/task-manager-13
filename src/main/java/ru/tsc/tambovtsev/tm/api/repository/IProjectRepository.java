package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    boolean existsById(String id);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

}